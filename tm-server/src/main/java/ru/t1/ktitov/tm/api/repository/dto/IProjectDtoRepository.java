package ru.t1.ktitov.tm.api.repository.dto;

import ru.t1.ktitov.tm.dto.model.ProjectDTO;

public interface IProjectDtoRepository extends IUserOwnedDtoRepository<ProjectDTO> {

}
