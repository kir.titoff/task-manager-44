package ru.t1.ktitov.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktitov.tm.api.endpoint.*;
import ru.t1.ktitov.tm.api.service.*;
import ru.t1.ktitov.tm.api.service.dto.*;
import ru.t1.ktitov.tm.api.service.model.*;
import ru.t1.ktitov.tm.endpoint.*;
import ru.t1.ktitov.tm.enumerated.Role;
import ru.t1.ktitov.tm.dto.model.UserDTO;
import ru.t1.ktitov.tm.service.*;
import ru.t1.ktitov.tm.service.dto.*;
import ru.t1.ktitov.tm.service.model.*;
import ru.t1.ktitov.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

@Getter
public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final IProjectDtoService projectService = new ProjectDtoService(connectionService);

    @NotNull
    private final ITaskDtoService taskService = new TaskDtoService(connectionService);

    @NotNull
    private final IProjectTaskDtoService projectTaskService = new ProjectTaskDtoService(connectionService);

    @NotNull
    private final IUserDtoService userService = new UserDtoService(connectionService, propertyService);

    @Getter
    @NotNull
    private final ISessionDtoService sessionService = new SessionDtoService(connectionService);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(propertyService, userService, sessionService);

    @Getter
    @NotNull
    private final IDomainService domainService = new DomainService(this);

    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NotNull
    private final IDomainEndpoint domainEndpoint = new DomainEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IAuthEndpoint authEndpoint = new AuthEndpoint(this);

    @NotNull
    private final Backup backup = new Backup(this);

    {
        registry(authEndpoint);
        registry(domainEndpoint);
        registry(projectEndpoint);
        registry(systemEndpoint);
        registry(taskEndpoint);
        registry(userEndpoint);
    }

    private void registry(@NotNull final Object endpoint) {
        @NotNull final String host = propertyService.getServerHost();
        @NotNull final String port = propertyService.getServerPort().toString();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = "http://" + host + ":" + port + "/" + name + "?wsdl";
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void initDemoData() {
        @NotNull final UserDTO test = userService.create("test", "test", "test@test.ru");
        @NotNull final UserDTO user = userService.create("user", "user", "user@user.ru");
        @NotNull final UserDTO admin = userService.create("admin", "admin", Role.ADMIN);

//        projectService.add(new Project("TEST", "DESC1", Status.COMPLETED));
//        projectService.add(new Project("DEMO", "DESC2", Status.NOT_STARTED));
//        projectService.add(new Project("BETA", "DESC3", Status.COMPLETED));
//        projectService.add(new Project("KAPPA", "DESC3", Status.IN_PROGRESS));

//        taskService.create(test.getId(), "TEST-TASK1", "TASKDESC1");
//        taskService.create(test.getId(), "DEMO-TASK2", "TASKDESC2");
//        taskService.create(test.getId(), "KAPPA-TASK3", "TASKDESC3");
    }

    private void prepareShutdown() {
        loggerService.info("** TASK MANAGER SERVER IS SHUTTING DOWN **");
        backup.stop();
    }

    public void run(@Nullable final String[] args) {
        initPID();
//        initDemoData();
        loggerService.info("** WELCOME TO TASK MANAGER SERVER **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
//        backup.start();
    }

}
