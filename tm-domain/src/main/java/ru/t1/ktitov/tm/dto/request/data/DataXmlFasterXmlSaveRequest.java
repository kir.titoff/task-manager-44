package ru.t1.ktitov.tm.dto.request.data;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktitov.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public final class DataXmlFasterXmlSaveRequest extends AbstractUserRequest {

    public DataXmlFasterXmlSaveRequest(@Nullable final String token) {
        super(token);
    }

}
